import React,{Fragment} from 'react';
import Navbar from './components/layouts/Navbar';
import './App.css';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Home from './components/layouts/pages/Home';
import About from './components/layouts/pages/About';
import StudentState from './components/layouts/student/StudentState';
import RegisterUser from './components/layouts/Register';
import LoginUser from './components/layouts/Login';
import UserAuth from './components/layouts/auth/AuthState';
import AuthToken from './components/layouts/auth/tokenAuth';
import PrivateRoute from "./components/layouts/auth/ProtectRoute";



if (localStorage.token){
      AuthToken(localStorage.token);
}

function App() {
  return (
    <UserAuth>
    <StudentState>
      <Router>
        <Fragment>
            <Navbar/>
            <Switch>
              < PrivateRoute exact path ='/' component={Home}/>
              <Route exact path ='/about' component={About}/>
              <Route exact path ='/register' component={RegisterUser}/>
              <Route exact path ='/login' component={LoginUser}/>
                
            </Switch>
        </Fragment>
      </Router>
    </StudentState>
    </UserAuth>
  );
}

export default App;
