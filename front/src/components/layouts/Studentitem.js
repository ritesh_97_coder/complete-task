import React, {useContext } from 'react';
import StudentContext from './student/StudentContext';

const Studentitem = ({student}) =>{
    const studentContext = useContext(StudentContext);
    const {deleteStudent,setCurrent,clearCurrent,updateStudent } = studentContext;
    const{_id,name,email,phone,password} = student;
    const onDelete = () => {
        deleteStudent(_id);
        clearCurrent();
    };
    return (
        <div>
             <div className="card mt-3.5">
                    <h5 className="card-header">{student.name}</h5>
                    <div className="card-body">
                      <p className="card-text">Email:{student.email}</p>
                      <p className="card-text">Phone:{student.phone}</p>
                      <p className="card-text">Password:{student.password}</p>
                      <a href="#" onClick={()=> setCurrent(student)}
                       className="btn btn-info">Edit</a>
                      <a href="#" onClick={onDelete} className="btn btn-danger ml-3">Delete</a>
                    </div>
                  </div>
        </div>
    )
}

export default Studentitem;