import React,{useState,useContext,useEffect} from 'react';

import AuthContext from './auth/AuthContext';

const Register = props =>{
    const authContext = useContext(AuthContext);

    const {registeruser,isAuthenticated } = authContext;

    useEffect(()=>{
        if(isAuthenticated){
            props.history.push("/");
        }
    },[isAuthenticated,props.history]);

    const [user, setuser] =useState({
        name: '',
        email:'',
        password:'',
        phone:'',
        cpassword:''
    });
    const {name,email,phone,password,cpassword} = user;

    const onChange = e => setuser({
        ...user,
        [e.target.name]: e.target.value
    });
    const onSubmit = e =>{
        e.preventDefault();
        registeruser({
            name,
            email,
            phone,
            password
        })
       
    };
    return (
        <form onSubmit={onSubmit}>
            <div className="row">
                <div className="col-sm-4 ml-5 mt-5 bg-light p-5 card">
                <h3>Register The Users</h3>
            <div className="form-group">
                <label>Enter Your Name</label>
                <input className="form-control" 
                type="text" 
                name="name"
                required
                value={name}
                onChange={onChange}/>
            </div>
            <div className="form-group">
                <label>Enter Your Email</label>
                <input className="form-control" 
                type="email" 
                name="email" 
                required
                value={email}
                onChange={onChange}/>
            </div>
            <div className="form-group">
                <label>Enter Your Phone</label>
                <input className="form-control" 
                type="text" 
                name="phone" 
                required
                value={phone}
                onChange={onChange}/>
            </div>
            <div className="form-group">
                <label>Enter Your Password</label>
                <input className="form-control" 
                type="password" 
                name="password" 
                required
                value={password}
                onChange={onChange}/>
            </div>
            <div className="form-group">
                <label>Confirm Your Password</label>
                <input className="form-control" 
                type="password" 
                name="cpassword" 
                required
                value={cpassword}
                onChange={onChange}/>
            </div>
            <input 
            type="submit" 
            value='Register Now'
            className="btn btn-success"
            />

                </div>
            </div>
            

        </form>

     ) 

}
export default Register;
