import React, { useContext ,useEffect} from 'react';
import AllStudents from '../AllStudent';
import StudentForm from '../StudentForm';
import AuthContext from "../auth/AuthContext";

const Home = props => {
    const authcontext = useContext(AuthContext);
    const {isAuthenticated, loadUser} = authcontext;
    useEffect(()=>{
        if(isAuthenticated){
          loadUser();
        }
        else{
            props.history.push('/Home')
        }
    },[isAuthenticated,props.history]);
    return(
        
        <div className="container">
            <div className="row">
            <div className="col-sm-7">
                <AllStudents/>
            </div>
            <div className="col-sm-5">
                <StudentForm />
            </div>
            </div>
        </div>
    );

};

export default Home;