import React, { useReducer } from 'react';
import uuid from 'react-uuid';
import StudentContext from './StudentContext';
import StudentReducer from './StudentReducer';
import axios from 'axios';
import{
    ADD_STUDENT,
    DELETE_STUDENT,
    UPDATE_STUDENT,
    SET_CURRENT_STUDENT,
    CLEAR_CURRENT_STUDENT,
    FILTER_STUDENT,
    CLEAR_FILTER_STUDENT,
    SET_ALERT,
    CLEAR_ALERT,
    GET_STUDENT
} from './actions';

const StudentState = props =>{
    const initialState ={
        students : [],
        current: null

    };
    const [state, dispatch] = useReducer(StudentReducer, initialState);
    /**Adding all actions here */

    /** getting all the students of respective users*/
    const getStudent = async ()=>{
        try{
            const res = await axios.get("/api/student");
            dispatch({type:GET_STUDENT,payload:res.data});
        }catch(err){

        }
    };
    /**adding the actions */
    const AddStudent = async student =>{
      const config = {
          headers:{
            'Content-Type':'application/json'     
           }
        };
        try{
          const res = await axios.post('/api/student', student, config)
          dispatch({ type: ADD_STUDENT,payload: student});
        }catch(err){
            console.log(err);

        }
};
    /**Deleting action */

    const deleteStudent = async id =>{
        try{
            await axios.delete(`/api/student/${id}`);
            dispatch({type:DELETE_STUDENT,payload:id});
        }catch(err){ 
        console.log(err);
        }   
    };

    const setCurrent = student =>{
        dispatch({type: SET_CURRENT_STUDENT, payload: student});
    };

    const clearCurrent = () =>{
        dispatch({type:CLEAR_CURRENT_STUDENT});

    };
    const updateStudent = async student =>{
        const config ={
            headers:{
                'Content-Type':'Application/json'
            }
        }
        try{
            const res = await axios.put(`/api/student/${student._id}`,student,config);
            dispatch({type: UPDATE_STUDENT, payload: res.data});
        }catch(err){
            console.log(err);
        }
    };

    return(
        <StudentContext.Provider
        value = {
            {
                students:state.students,
                current:state.current,
                AddStudent,
                deleteStudent,
                setCurrent,
                clearCurrent,
                updateStudent,
                getStudent

            }
        }>
            { props.children }
        </StudentContext.Provider>
    );
    
}

export default StudentState;
