export const ADD_STUDENT = "ADD_STUDENT";
export const DELETE_STUDENT = "DELETE_STUDENT";
export const UPDATE_STUDENT = "UPDATE_STUDENT";
export const SET_CURRENT_STUDENT = "SET_CURRENT_STUDENT";
export const FILTER_STUDENT = "FILTER_STUDENT";
export const CLEAR_FILTER_STUDENT = "CLEAR_FILTER_STUDENT";
export const SET_ALERT = "SET_ALERT";
export const CLEAR_ALERT = "CLEAR_ALERT";
export const CLEAR_CURRENT_STUDENT = "CLEAR_CURRENT_STUDENT"
export const REGISTER_USER = "REGISTER_USER";
export const LOGIN_USER = "LOGIN_USER";
export const LOGOUT = "LOGOUT";
export const REGISTRATION_FAILED = "REGISTRATION_FAILED";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOAD_USER = "LOAD_USER";
export const AUTH_ERROR = "AUTH_ERROR";
export const GET_STUDENT = "GET_STUDENT";