import{
    ADD_STUDENT,
    DELETE_STUDENT,
    UPDATE_STUDENT,
    SET_CURRENT_STUDENT,
    CLEAR_CURRENT_STUDENT,
    FILTER_STUDENT,
    CLEAR_FILTER_STUDENT,
    SET_ALERT,
    CLEAR_ALERT,
    GET_STUDENT
} from './actions';

export default (state, action) =>{
    switch(action.type){
        case GET_STUDENT:
            return{
                ...state,
                students: action.payload,
                loading:false
            }
        case ADD_STUDENT:
            return{
                ...state,
                students: [...state.students, action.payload],
                loading:false
            };
            case DELETE_STUDENT:
                return{ 
                    ...state,
                    students: state.students.filter(
                        student => student._id !== action.payload
                    ),
                    loading:false
                };
            case SET_CURRENT_STUDENT:
                return{
                    ...state,
                    current:action.payload
                };
            case CLEAR_CURRENT_STUDENT:
                return {
                    ...state,
                    current:null
                };
            case UPDATE_STUDENT:
                return{
                    ...state,
                    students: state.students.map(student =>
                    student._id === action.payload._id ? action.payload : student
                    ),
                    loading:false
                }
            default:
                return state;
    }
};