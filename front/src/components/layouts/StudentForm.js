import React, {useState,useContext, useEffect} from 'react';
import StudentContext from './student/StudentContext';


const StudentForm=()=>{
    const studentContext = useContext(StudentContext);
    const { AddStudent,current,clearCurrent, updateStudent } = studentContext;
    useEffect(()=>{
        if(current !==null)
        {
            setstudent(current);
        }
        else
        {
            setstudent({ 
                name:"",
                email:"",
                phone:"",
                password:""
                   
                });
        }

    },[studentContext, current]);
    const [student,setstudent] = useState({
        name:"",
        email:"",
        phone:"",
        password:""
       
    });
    const { name,email,phone,password} = student;

    const onChange = e => setstudent({
        ...student,
        [e.target.name]:e.target.value
    });
    const onSubmit = e =>{
        e.preventDefault();
        if (current === null) {
            AddStudent(student);
        }
        else
        {
            updateStudent();
        }
    };

    const clearAll = ()=>{
        clearCurrent();

    };

    return (
        <div>
        <h3 className ="alert alert-info">{current ? "Edit The User" : "Add The User"}</h3>
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <label>Enter Your Name</label>
            <input className="form-control" type="text" name="name" value={name}
                onChange={onChange}/></div>
            <div className="form-group">
            <label>Enter Your Email</label>
                         <input className="form-control" type="email" name="email" value={email}
                onChange={onChange}/></div>
            <div className="form-group">
            <label>Enter Your Phone number</label>
                      <input className="form-control" type="text" name="phone" value={phone}
                            onChange={onChange}/></div>
            <div className="form-group">
            <label>Enter Your Password</label>
                    <input className="form-control" type="text" name="password" value={password}
                        onChange={onChange}/>
            </div>
            <input type="submit" value={current ? "Update Now" : "Save Now"} className="btn btn-success"/>
               {current && (
                <input
                type="button"
                value="Clear Now" 
                onClick ={clearAll}
                className="btn btn-warning ml-5"
                />
            )
           }
        </form>
        </div>
    )
}

export default StudentForm;