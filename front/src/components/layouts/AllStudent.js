import React,{ Fragment, useContext,useEffect } from 'react';
import StudentContext from './student/StudentContext';
import StudentItems from './Studentitem';


const AllStudent =() =>{
    const studentContext = useContext(StudentContext);
    const {students,getStudent,loading} = studentContext;
    useEffect(()=>{
        getStudent();
        
    },[])
    if(students.length === 0){
        return <h3 className='alert alert-danger'>Add New User</h3>
    }
        return(
            <Fragment>
                {students.map(student =>(
                    <StudentItems key={student.id} student= {student}/>
                   
                ))}

            </Fragment>
        
    )
}

export default AllStudent;