import React,{useContext,useEffect,Fragment } from 'react';
import {Link } from 'react-router-dom';
import authContext from "../layouts/auth/AuthContext";



const Navbar = () =>{
  const authcontext = useContext(authContext);
  const{user,isAuthenticated,logout} = authcontext;
  const Exit = ()=>{
    logout();
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
  const authlink = (
    <Fragment>
      <li className="nav-item active">
        <Link className="nav-link" to="/">Home</Link>
      </li>
      <li className="nav-item active">
        <Link className="nav-link" to="/">
          {user && user.name}
        </Link>
      </li>
      <li className="nav-item active">
        <a onClick={Exit} className="nav-link" href="/Home">
          Logout</a>
      </li>

    </Fragment>
  );
  const unauthlink = (
    <Fragment>
       <li className="nav-item">
        <Link className="nav-link" to="/about">About</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="/register">Register</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="/login">Login</Link>
      </li>

    </Fragment>

  );
    return(
      <div className='navbar navbar-expand-lg navbar-dark'
      style={{backgroundColor: "#7952b3"}}>
        <a className='navbar-brand' href="#">
          CRUD_USERS
        </a>
        <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarNav'
        aria-controls='navbarNav'
        aria-aria-expanded='false'
        aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>
        <div className='collapse navbar-collapse' id='navbarNav'>
           <ul className='navbar-nav ml-auto'>
             {isAuthenticated ? authlink : unauthlink }
           </ul>
   
              </div>
        </div>
    );
};

export default Navbar;