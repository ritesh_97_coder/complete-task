const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function(req,res,next){
    /**got the token */
    const token = req.header('x-auth-token');
    if(!token){
        return res.status(401).json({msg:'No any Token Found...'});
    }
    try{
        const decodetoken = jwt.verify(token,config.get('Secretkey'));
        req.user = decodetoken.user;
        next();
    }catch(err){
        res.status(401).json({msg:'Inavalid/Unautharized Token'});
    }
}