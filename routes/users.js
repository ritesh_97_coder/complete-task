const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

const { check, validationResult } = require('express-validator');

const UserModel = require('../models/UserModel');

/**register user route**/

router.post('/',[
    check('name','name is required').not().isEmpty(),
    check('email','please enter valid email').isEmail(),
    check('phone','please enter valid phone').isLength({
        min:10
    }),
    check('password','Please password with min 8 or more Characters').isLength({
        min:8})
    
], async (req,res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }
    const {name,email,phone,password} = req.body;
    try{
        let user = await UserModel.findOne({email});
        if(user){
            return res.status(400).json({msg: 'User alraedy exist with this email'});
        }
        /**new user data saved */
        user = new UserModel({
            name,
            email,
            phone,
            password
        })
    /**hashed password conversion */
    const salt  = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();
    const payload = {
        user: {
            id: user.id
        }
    };
    jwt.sign(payload, config.get('Secretkey'),{
        expiresIn:'3h'
    },(err,token)=>{
        if(err) throw err;
        res.json({token});
    })
    
}catch(error){

}
});

module.exports = router;