const express = require('express');
const router = express.Router();
const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const UserModel = require('../models/UserModel');
const { check, validationResult } = require('express-validator');
const { route } = require('./users');
const auth = require('../middlewares/auth');

router.get('/', auth, async (req,res)=>{
    try {
        const user = await (await UserModel.findById(req.user.id)).isSelected('-password');
        res.json(user);
    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server error');
    }
});

router.post('/',[
    check('email','Please enter  valid email').isEmail(),
    check('password','Please enter your password').exists()
],
async (req,res) =>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }
    const {email,password} = req.body;
    try {
        let user = await UserModel.findOne({email});
        if(!user){
            return res.status(400).json({msg: 'User does not exist with this email'});
        }
        const checkpassword = await bcrypt.compare(password, user.password);
        if(!checkpassword){
            return res.status(400).json({msg:'Invalid/Wrong password'});
        }
        const payload = {
            user: {
                id: user.id
            }
        };
        jwt.sign(payload, config.get('Secretkey'),{
            expiresIn:'3h'
        },(err,token)=>{
            if(err) throw err;
            res.json({token});
        })

    } catch (error) {
        console.log(error.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;