const express = require('express');
const router = express.Router();
const {check, validationResult} = require('express-validator');
const UserModel = require('../models/UserModel');
const StudentModel = require('../models/StudentModel');
const auth = require('../middlewares/auth');


/**all get data of students */
router.get('/', auth, async (req,res)=>{
    try{
        const students = await StudentModel.find({user:req.user.id}).sort({date: -1});
        res.json(students);
    }catch(err){
        console.log(err.message);
        res.status(500).send('Server error');

    }
});

/**save  data of students */
router.post('/',[auth,
    check('name','name is required').not().isEmpty(),
    check('email','please enter valid email').isEmail(),
    check('phone','please enter valid phone').isLength({
        min:10
    }),
    check('password','Please password with min 8 or more Characters').isLength({
        min:8})
],async (req,res)=>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }
    const {name,email,phone,password} = req.body;
    try {
        const newStudent = new StudentModel({
            name,
            email,
            phone,
            password,
            user:req.user.id
        });
        const savedstudent = await newStudent.save();
        res.json(savedstudent);
    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server Error');
        
    }
});


/**Edit data of students */
router.put('/:id', auth, async (req,res)=>{
    const {name,email,phone,password} = req.body;
    const studentfields = {};
    if(name) studentfields.name = name;
    if(email) studentfields.email = email; 
    if(phone) studentfields.phone = phone;
    if(password) studentfields.password = password;
    try {
        let student = await StudentModel.findById(req.params.id);
        if(!student){
            return res.status(404).json({msg:'student not found....'})
        }
        student = await StudentModel.findByIdAndUpdate(req.params.id,{
            $set: studentfields
        }, {new:true});
        res.json(student);

    } catch (err) {
        console.log(err.message);
        res.status(500).send('Server Error');
        
    }
});


/**Delete data of students */
router.delete('/:id', auth,async (req,res)=>{
    try{
        let student = await StudentModel.findById(req.params.id);
        if(!student){
            return res.status(401).json({msg:'Data Not found'});
        }
        await StudentModel.findByIdAndDelete(req.params.id);
        res.json({msg:'Data has deleted'});
   
    }catch(err){
        console.log(err.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;